package com.hubcba.ironhide.mockservice.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hubcba.ironhide.mockservice.model.BaseRequest;
import com.hubcba.ironhide.mockservice.model.CellphoneVerificationRequest;
import com.hubcba.ironhide.mockservice.model.CellphoneVerificationResponse;
import com.hubcba.ironhide.mockservice.model.SoapHeaderMock;
import com.hubcba.ironhide.mockservice.service.MockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.endpoint.annotation.SoapHeader;

import javax.jws.WebMethod;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;

@Endpoint
public class MockEndpoint {
    public static final String NAMESPACE_URI = "http://tempuri.org/";

    private MockService mockService;

    @Autowired
    public MockEndpoint(MockService mockService) {
        this.mockService = mockService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "cellphoneVerificationRequest")
    @ResponsePayload
    public CellphoneVerificationResponse verifyCellphoneNumber(@RequestPayload CellphoneVerificationRequest cellphoneVerificationRequest) {
        String path = "verification";
        String appendPath = "/cellphone/" + cellphoneVerificationRequest.getCellphone();
        String json = mockService.getResponseCode(
                path,
                "POST",
                appendPath);
        ObjectMapper mapper = new ObjectMapper();
        CellphoneVerificationResponse response = new CellphoneVerificationResponse();
        try {
            response = mapper.readValue(json, CellphoneVerificationResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }


}
