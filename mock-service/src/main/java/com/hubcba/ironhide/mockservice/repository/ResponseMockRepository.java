package com.hubcba.ironhide.mockservice.repository;

import com.hubcba.ironhide.mockservice.domain.ResponseMock;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResponseMockRepository extends MongoRepository<ResponseMock, String> {
    ResponseMock findByPathAndMethodAndAppendPath(String path, String method, String appendPath);
}
