package com.hubcba.ironhide.mockservice.model;

import com.hubcba.ironhide.mockservice.endpoint.MockEndpoint;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "cellphoneVerificationRequest", namespace = MockEndpoint.NAMESPACE_URI)
@XmlType(name = "cellphoneVerificationRequest", namespace = MockEndpoint.NAMESPACE_URI, propOrder = {
        "cellphone"})
@XmlAccessorType(XmlAccessType.FIELD)
public class CellphoneVerificationRequest extends BaseRequest {

    @XmlElement(name = "cellphone")
    private String cellphone;

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }
}
