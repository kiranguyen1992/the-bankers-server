package com.hubcba.ironhide.mockservice.model;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;

public class BaseResponse implements Serializable {
    private String statusCode;
    private String statusDescription;

    public @XmlElement
    String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public @XmlElement
    String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }
}
