package com.hubcba.ironhide.mockservice;

import com.hubcba.ironhide.mockservice.util.ExecuteShellCommand;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.io.File;

@EnableMongoRepositories
@SpringBootApplication
public class MockServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MockServiceApplication.class, args);
        ExecuteShellCommand.runScript("run_script_insert_sample.sh");
    }

}
