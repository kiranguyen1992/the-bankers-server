package com.hubcba.ironhide.mockservice.configuration;

import com.hubcba.ironhide.mockservice.endpoint.MockEndpoint;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

    @Bean(name = "mockService")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema mockSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("MockService");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace(MockEndpoint.NAMESPACE_URI);
        wsdl11Definition.setSchema(mockSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema mockServiceSchema() {
        return new SimpleXsdSchema(new ClassPathResource("mockservice.xsd"));
    }

}