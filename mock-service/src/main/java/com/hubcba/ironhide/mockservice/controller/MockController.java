package com.hubcba.ironhide.mockservice.controller;

import com.hubcba.ironhide.mockservice.domain.ResponseMock;
import com.hubcba.ironhide.mockservice.service.MockService;
import com.hubcba.ironhide.mockservice.util.ExecuteShellCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class MockController {

    @Autowired
    private MockService mockCellphoneService;

    @RequestMapping(value = "/**/*",
            method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
    @ResponseBody
    public String getData(HttpServletRequest req) throws IOException {
        String uri = req.getRequestURI();
        String type = req.getMethod();
        String removeFirst = uri.replaceFirst("/", "");
        String path = removeFirst.substring(0, removeFirst.indexOf("/"));
        String appendPath = removeFirst.substring(removeFirst.indexOf("/"), removeFirst.length());
        return mockCellphoneService.getResponseCode(path, type, appendPath);
    }

    @RequestMapping(value = "/insert",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseMock getData(@RequestBody ResponseMock request) throws IOException {
        return mockCellphoneService.insertData(request);
    }

    @RequestMapping(value = "/clear",
            method = RequestMethod.GET)
    @ResponseBody
    public void getData() {
        ExecuteShellCommand.runScript("run_script_insert_sample.sh");
    }

}
