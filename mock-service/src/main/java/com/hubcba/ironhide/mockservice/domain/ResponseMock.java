package com.hubcba.ironhide.mockservice.domain;

import org.springframework.data.annotation.Id;

public class ResponseMock {
    @Id
    private String id;
    private String path;
    private String method;
    private String appendPath;
    private String response;
    private long delay = 0;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppendPath() {
        return appendPath;
    }

    public void setAppendPath(String appendPath) {
        this.appendPath = appendPath;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }
}
