package com.hubcba.ironhide.mockservice.service;

import com.hubcba.ironhide.mockservice.domain.ResponseMock;

/**
 * Provides needed services to work with Origination Applications
 */
public interface MockService {

    /**
     * Check is existed cellphone number
     *
     * @param path for the link of purpose
     * @param method for the type of crud
     * @param appendPath for the data mock
     *
     * @return true or false
     */
    String getResponseCode(String path, String method, String appendPath);

    ResponseMock insertData(ResponseMock request);

}