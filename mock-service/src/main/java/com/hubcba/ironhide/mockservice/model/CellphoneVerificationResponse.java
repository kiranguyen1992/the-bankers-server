package com.hubcba.ironhide.mockservice.model;

import com.hubcba.ironhide.mockservice.endpoint.MockEndpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "cellphoneVerificationResponse", namespace = MockEndpoint.NAMESPACE_URI)
@XmlType(name = "cellphoneVerificationResponse", namespace = MockEndpoint.NAMESPACE_URI)
@XmlAccessorType(XmlAccessType.FIELD)
public class CellphoneVerificationResponse extends BaseResponse {

}
