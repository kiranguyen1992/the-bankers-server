package com.hubcba.ironhide.mockservice.util;

public enum ResponseStatusKey {
    /**
     * Cellphone is existed response status key.
     */
    CELLPHONE_IS_EXISTED,
    /**
     * Success response status key.
     */
    SUCCESS,
    /**
     * Ssn isn't existed response status key.
     */
    SSN_NOT_EXISTED,
    /**
     * Unknown error
     */
    UNKNOWN
}
