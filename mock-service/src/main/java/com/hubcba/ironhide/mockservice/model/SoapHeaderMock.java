package com.hubcba.ironhide.mockservice.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace = SoapHeaderMock.AUTH_NS)
public class SoapHeaderMock {

    public static final String AUTH_NS = "http://www.commbank.com.vn/security";

    private String path;
    private long timeDelay;
    private String key;

    public SoapHeaderMock(){}

    public SoapHeaderMock(String path, String key, long timeDelay){
        this.path = path;
        this.key = key;
        this.timeDelay = timeDelay;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getTimeDelay() {
        return timeDelay;
    }

    public void setTimeDelay(long timeDelay) {
        this.timeDelay = timeDelay;
    }
}
