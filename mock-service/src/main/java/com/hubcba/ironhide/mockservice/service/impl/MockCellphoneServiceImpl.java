package com.hubcba.ironhide.mockservice.service.impl;

import com.hubcba.ironhide.mockservice.domain.ResponseMock;
import com.hubcba.ironhide.mockservice.repository.ResponseMockRepository;
import com.hubcba.ironhide.mockservice.service.MockService;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class MockCellphoneServiceImpl implements MockService {

    private ResponseMockRepository responseMockRepository;

    @Autowired
    public MockCellphoneServiceImpl(ResponseMockRepository responseMockRepository) {
        this.responseMockRepository = responseMockRepository;
    }

    @Override
    public String getResponseCode(String path, String method, String appendPath) {
        final String[] response = {null};
        ResponseMock responseMock = responseMockRepository.findByPathAndMethodAndAppendPath(path, method, appendPath);
        Observable.just(path, method, appendPath)
                .delay(responseMock.getDelay() / 2, TimeUnit.MILLISECONDS, Schedulers.trampoline())
                .subscribe(s -> {
                    response[0] = responseMock.getResponse();
                });
        return response[0];
    }

    @Override
    public ResponseMock insertData(ResponseMock request) {
        return responseMockRepository.save(request);
    }
}
