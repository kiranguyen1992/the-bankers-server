package com.hubcba.ironhide.mockservice.util;

public class ExecuteShellCommand {
    public static void runScript(String fileName){
        try {
            ProcessBuilder pb = new ProcessBuilder(System.getProperty("user.dir") + "/mock-service/src/main/resources/" + fileName);
            Process p = pb.start();     // Start the process.
            p.waitFor();                // Wait for the process to finish.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
